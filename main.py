import sys
from datetime import datetime, timezone

from requests import HTTPError

from utils import get_existing_results, save_results


def main(analytics_token, api_url):
    try:
        # Get existing results
        existing_results = get_existing_results(api_url, analytics_token)

        my_engine_results = existing_results["my_engine"]

        # Built results array
        result = [
            {
                "column1": result["result"][0]["column1"],
                "column2": result["result"][0]["column2"],
            }
            for result in my_engine_results
        ]

        description = {
            "de": "Diese Analyse summiert sämtliche gespeicherten xAPI Statements, die im Primären-LRS gespeichert sind",
            "en": "This analysis sums all stored xAPI statements stored in the primary LRS",
        }

        # Send result to rights engine
        save_results(
            api_url, analytics_token, {"result": result, "description": description}
        )
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
